# \<copy-text\>

[![Pipeline status][pipeline-badge]][pipeline]
[![Published on NPM][npm-badge]][npm]
[![MIT License][license-badge]][license]

Element that copy text (or current URL by default) then notify with a `paper-toast`

- [Install](#install)
	- [Install with Yarn](#install-with-yarn)
	- [Install with NPM](#install-with-npm)
		- [Simple install](#simple-install)
		- [Manual install](#manual-install)
	- [Install with Bower](#install-with-bower)
- [Usage](#usage)
	- [Simplest usage](#simplest-usage)
	- [Full usage](#full-usage)
- [License](#license)


## Install

### Install with Yarn

Using [Yarn][yarn]

```sh
$ yarn add copy-text
```


### Install with NPM

#### Simple install

```sh
$ npm install copy-text
```


#### Manual install

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ npm install copy-text --no-optional
```


### Install with Bower

First, make sure you have [Bower][bower] and [Polymer CLI][polymer-cli] installed

```sh
$ bower install copy-text --save
$ npm run-script analyze # if you need to watch the documentation
```


## Usage

### Simplest usage

<!--
```html
<custom-element-demo>
	<template>
		<link rel="import" href="copy-text.html" />
		<next-code-block></next-code-block>
	</template>
</custom-element-demo>
```
-->

```html
<copy-text></copy-text>
```

### Full usage

<!--
```html
<custom-element-demo>
	<template>
		<next-code-block></next-code-block>
	</template>
</custom-element-demo>
```
-->

```html
<copy-text
	text="This is not an URL"
	message-on-success="Text copied with success"
	message-on-error="Oops! Something goes wrong while copying"
>
	<button type="button">Click to copy</button>
</copy-text>
```


## License

[MIT Licence][license]



[pipeline-badge]: https://gitlab.com/pinage404/copy-text/badges/master/pipeline.svg
[pipeline]:       https://gitlab.com/pinage404/copy-text/commits/master

[npm-badge]: https://img.shields.io/npm/v/@pinage404/copy-text.svg
[npm]:       https://www.npmjs.com/package/@pinage404/copy-text

[license-badge]: https://img.shields.io/badge/license-MIT-green.svg
[license]:       https://gitlab.com/pinage404/copy-text/blob/master/LICENSE

[yarn]: https://yarnpkg.com

[bower]:       https://bower.io/#install-bower
[polymer-cli]: https://www.npmjs.com/package/polymer-cli
